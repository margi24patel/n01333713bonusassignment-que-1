﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment_1_WebApplication_1_
{
    public partial class Palindrome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        }


        protected void Result(object sender, EventArgs e)
        {
           
            string s1 = txtInputString.Text.ToString().ToLower();
            string s = s1.Replace(" ", string.Empty);
            string reverse = "";
            //string revs = txtInputString.Text.ToString().ToLower();

            for (int i = s.Length - 1; i >= 0; i--)
            {
                reverse += s[i].ToString();
            }

            if (reverse == s)
            {
                shownResult.InnerHtml = "This String is Palindrome!";
            }
            else
            {
                shownResult.InnerHtml = "This String is not Palindrome!";
            }

        }


    }
}