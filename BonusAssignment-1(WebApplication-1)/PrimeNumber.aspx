﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrimeNumber.aspx.cs" Inherits="BonusAssignment_1_WebApplication_1_.PrimeNumber" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server" ID="lblInteger" Text="Enter an Integer"></asp:Label>
            <asp:TextBox runat="server" ID="txtInteger"></asp:TextBox>
            <asp:RequiredFieldValidator ID="requireInteger" runat="server" ControlToValidate="txtInteger" ErrorMessage="Enter an Integer Value"></asp:RequiredFieldValidator>

            <br />

            <asp:Button runat="server" ID="btnCheckPrime" Text="Check if Prime" OnClick="CheckifPrime" />
            <br />
            <div id="shownResult" runat="server"></div>

        </div>
    </form>
</body>
</html>
