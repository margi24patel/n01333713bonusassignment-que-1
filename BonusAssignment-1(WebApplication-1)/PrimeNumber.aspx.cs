﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment_1_WebApplication_1_
{
    public partial class PrimeNumber : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CheckifPrime(object sender, EventArgs e)
        {
            int i, a, b = 0, flag = 0;

            a = int.Parse(txtInteger.Text);

            b = a / 2;

            for(i = 2; i <= b; i++)
            {
                if(a % i == 0)
                {
                    shownResult.InnerHtml = "The number is Not Prime!";
                    flag = 1;
                    break;
                }
            }

            if(flag == 0)
            {
                shownResult.InnerHtml = "The number is Prime!";
            }



        }
    }
}