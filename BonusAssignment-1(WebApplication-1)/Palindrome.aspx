﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Palindrome.aspx.cs" Inherits="BonusAssignment_1_WebApplication_1_.Palindrome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:Label runat="server" ID="lblInputString" Text="Input a String of Characters : "></asp:Label> 
            <asp:TextBox runat="server" ID="txtInputString"></asp:TextBox> 
            
            <asp:RequiredFieldValidator runat="server" ID="requireInputString" ControlToValidate="txtInputString" ErrorMessage="Enter String of Characters"></asp:RequiredFieldValidator>

            <asp:RegularExpressionValidator runat="server" ID="regularexpressionInputString" ControlToValidate="txtInputString" ValidationExpression="^[a-zA-Z ]+$" ErrorMessage="Enter Input As a String of Characters"></asp:RegularExpressionValidator>
            <br />

            <asp:Button runat="server" ID="btnCheckPalindrome" Text="Check if Palindrome" OnClick="Result" />

            <div runat="server" id="shownResult"></div>


        </div>
    </form>
</body>
</html>
