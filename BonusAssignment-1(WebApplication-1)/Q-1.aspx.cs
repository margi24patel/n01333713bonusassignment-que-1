﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment_1_WebApplication_1_
{
    public partial class Q_1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Result(object sender, EventArgs e)
        {


            int x = int.Parse(txtXvalue.Text);
            int y = int.Parse(txtYvalue.Text);

            Quadrant newquadrant = new Quadrant(x, y);



            if (x > 0 && y > 0)
            {
                showresult.InnerHtml = "({" + x + " } , { " + y + " }) in First Quadrant";

            }

            else if (x < 0 && y > 0)
            {
                showresult.InnerHtml = "({" + x + " } , { " + y + " }) in Second Quadrant";

            }

            else if (x < 0 && y < 0)
            {
                showresult.InnerHtml = "({" + x + " } , { " + y + " }) in Third Quadrant";

            }

            else if (x > 0 && y < 0)
            {
                showresult.InnerHtml = "({" + x + " } , { " + y + " }) in Fourth Quadrant";

            }





        }

    }
}