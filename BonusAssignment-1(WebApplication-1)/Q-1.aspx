﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Q-1.aspx.cs" Inherits="BonusAssignment_1_WebApplication_1_.Q_1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:Label ID="Xvalue" runat="server" Text="X Value :"></asp:Label>
            <asp:TextBox ID="txtXvalue" runat="server" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="requireXvalue" ControlToValidate="txtXvalue" ErrorMessage="Enter X-cordinate Value"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="comparevalidatorXvalue" runat="server" ControlToValidate="txtXvalue" Type="Integer" Operator="NotEqual" ValueToCompare="0" ErrorMessage="Can't be 0!"></asp:CompareValidator>
            
            <br />
            
            <asp:Label ID="Yvalue" runat="server" Text="Y Value :"></asp:Label>
            <asp:TextBox ID="txtYvalue" runat="server" ></asp:TextBox>

            
            <asp:RequiredFieldValidator runat="server" ID="requireYvalue" ControlToValidate="txtYvalue" ErrorMessage="Enter Y-cordinate Value"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="comparevalidatorYvalue" runat="server" ControlToValidate="txtYvalue" Type="Integer" Operator="NotEqual" ValueToCompare="0" ErrorMessage="Can't be 0!"></asp:CompareValidator>
            <br />

            <div runat="server" id="showresult"></div>

            <asp:Button runat="server" ID="submitCordinate" Text="Submit" OnClick="Result" />


        </div>
    </form>
</body>
</html>
